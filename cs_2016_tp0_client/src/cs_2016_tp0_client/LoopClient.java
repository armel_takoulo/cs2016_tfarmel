package cs_2016_tp0_client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

import net_utils.HostAdress;

public class LoopClient {

	static String serverUri = "192.168.1.95"; // si serveur sur une autre
												// machine
	// static String serverUri = "127.0.0.1"; //127.0.0.1 si serveur sur son
	// propre poste

	static int port = 8053;

	private static void delai(int millis) {
		if (millis > 0)
			try {
				Thread.sleep(millis);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	}

	public static void main(String[] args) throws IOException {
		// InetAddress myadress = InetAddress.getLocalHost();
		String myadress = HostAdress.getHostAddress();
		System.out.println("Je suis " + myadress);
		BufferedReader userIn = new BufferedReader(new InputStreamReader(
				System.in));
		while (true) {
			System.out.println("Tapez une commande");
			String requete = userIn.readLine();
			if ("stop".equals(requete))
				break;
			System.out.println("Tentative de connexion au serveur " + serverUri
					+ " " + port);
			Socket socket = new Socket(serverUri, port);
			System.out.println("Je suis connect� au serveur " + serverUri + " "
					+ port);
			BufferedReader entree = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			PrintStream sortie = new PrintStream(socket.getOutputStream());
			sortie.println(requete);
			System.out.println("j'attends la r�ponse du serveur");
			String reponse = entree.readLine();
			System.out.println("Le serveur a r�pondu: [" + reponse + "]");
			entree.close();
			sortie.close();
			socket.close();
			delai(50);
		}
		System.out.println("c'est fini");
	}

}
