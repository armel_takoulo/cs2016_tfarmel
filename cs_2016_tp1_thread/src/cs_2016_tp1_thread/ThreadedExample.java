package cs_2016_tp1_thread;

import java.util.ArrayList;
import java.util.List;

//see http://puredanger.github.io/tech.puredanger.com/2009/06/08/interrupt-handling/
//see http://rom.developpez.com/java-synchronisation/
/**
 * 
 * @author pfister
 *
 */
public class ThreadedExample {
	private final static String TABS = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
	private static final boolean PRINT_EXCEPTION = true;

	List<MyTask> tasks = new ArrayList<ThreadedExample.MyTask>();

	private boolean globalEnd;

	private Object lock = new Object();

	private void log(String mesg) {
		synchronized (lock) {
			for (int i = 0; i < mesg.length(); i++)
				System.out.print(mesg.charAt(i));
			System.out.print('\n');
		}
	}

	private void delay(int millis) {
		if (millis > 0)
			try {
				Thread.sleep(millis);
			} catch (InterruptedException e) {
				if (PRINT_EXCEPTION)
					log("!!!" + e.toString());
			}
	}

	public void go() {
		int n = 0;
		log("go ThreadedExample");
		while (true) {
			log("loop " + n);
			MyTask newtask = new MyTask(n++);
			tasks.add(newtask);
			newtask.start();
			delay(2000);
			if (n == 10) {
				globalEnd = true;
				break;
			}
		}
		log("end ThreadedExample");
	}

	class MyTask extends Thread {
		int id;
		int tic;
		boolean interrupted;

		public MyTask(int id) {
			this.id = id;
		}

		@Override
		public void interrupt() {
			super.interrupt();
			interrupted = true;
			log("----- the task " + id + " is interrupted");
		}

		@Override
		public void run() {
			log("the task " + id + " starts");
			while (!globalEnd && !interrupted) {
				log(TABS.substring(0, id + 1) + " task " + id + " tic " + tic++);
				if (id == 4) {
					if (tic > 10 && tic < 600)
						delay(200);
					if (tic == 50) {
						Thread t0 = tasks.get(0);
						if (!((MyTask) t0).interrupted) {
							log("###### interrupt the task t0 !");
							t0.interrupt();
						}
					}
				} else
					delay(1000);
			}
			log("the task " + id + " ends");
		}
	}

	public static void main(String[] args) {
		ThreadedExample example = new ThreadedExample();
		example.go();
	}
}